# RetoAutomationQA - Sofka
#**Test Automation - Screenplay Pattern - API - SerenityRest**

## Pre-requisitos
1. Java 8
2. Gradle 5.2.1 o Superior
3. IDE de desarrollo
	- b) IntelliJ
4. Dependencias: se deben definir en el archivo **build.gradle**
	
```java
   ext {
   serenityVersion = "3.0.1"
   serenityGradleVersion = "2.4.34"
   serenityCucumberVersion = "3.0.1"
   slf4jVersion = '1.7.30'
   junitVersion = '4.13'
   assertJVersion = '3.8.0'
   javaFakerVersion = '1.0.2'

}

dependencies {
implementation group: 'net.serenity-bdd', name: 'serenity-cucumber', version: "$rootProject.ext.serenityCucumberVersion"
implementation group: 'net.serenity-bdd', name: 'serenity-junit', version: "$rootProject.ext.serenityVersion"
implementation group: 'net.serenity-bdd', name: 'serenity-core', version: "$rootProject.ext.serenityVersion"
implementation group: 'net.serenity-bdd', name: 'serenity-screenplay', version: "$rootProject.ext.serenityVersion"
implementation group: 'net.serenity-bdd', name: 'serenity-screenplay-webdriver', version: "$rootProject.ext.serenityVersion"
implementation group: 'net.serenity-bdd', name: 'serenity-screenplay-rest', version: "$rootProject.ext.serenityVersion"
implementation group: 'org.slf4j', name: 'slf4j-simple', version: "$rootProject.ext.slf4jVersion"
implementation group: 'junit', name: 'junit', version: "$rootProject.ext.junitVersion"
implementation group: 'com.github.javafaker', name: 'javafaker', version: "$rootProject.ext.javaFakerVersion"
implementation group: 'com.googlecode.json-simple', name: 'json-simple', version: '1.1.1'

}
````

## Descripción

Este es un reto de automatización de API REST, se encuentra creado bajo el patrón de diseño Screenplay e integra varias herramientas que incluyen a SerenityBDD y Cucumber dentro del manejador de proyecto Gradle.

La estructura del proyecto está conformada por paquetes nombrados de la siguiente manera:

![img.png](src/test/resources/images/img.png)

---
- ☕📦️ **interactions**:
  Clases que representan las interacciones directas con la API, donde está implementada la lógica de programación para consumir los métodos GET, PUT y DELETE.
  
- ☕📦️ **model**:
  Clases relacionadas con el modelo de dominio.
  
- ☕📦️ **tasks**:
  Clases que representan tareas que realiza el actor en el ámbito de proceso de negocio.
  
- ☕📦️ **questions**:
  Objetos usados para consultar acerca del estado de la aplicación o verificar el resultado esperado de la ejecución de prueba.
  
- ☕📦️ **util**:
  Clases de utilidad.
  
- ☕📦️ **runners**
  Clases que permiten correr los tests.
  
- ☕📦️ **step definitions**
  Clases que mapean las líneas Gherkin a código java y donde se llaman todas las tareas(tasks) y preguntas(questions) creadas para la ejecución de la prueba.
  
- ☕📦️ **features**:
  La representación de las historias de usuarios en archivos en lenguaje gherkin (.feature).

---
  
**Este proyecto cuenta con los siguientes scenarios:**

  ##Escenario:
   ```gherkin
    @AllTests
Feature: Verify user API
	As a user
	I want to access reqres.in
	And manage the user API information

	Background: User exists
		Given A list of users exists

	@CheckUserId
	Scenario Outline: Check query of an existing user by ID
		When I consult the user by ID "<id>"
		Then the user details should be returned
			|id|firstName|lastName|email|
			|<id>|<firstName>|<lastName>|<email>|

		Examples:
			|id|firstName|lastName|email|
			|1 |George|Bluth|george.bluth@reqres.in|


	@CheckUpdateUser
	Scenario Outline: Check update the update of a user's data
		When I update the user with ID "<id>" with the following details
			|id| firstName| lastName| email |
			|<id>| <firstName> | <lastName>| <email> |
		Then the user details should be
			|id| firstName| lastName| email |
			|<id>| <firstName> | <lastName>| <email> |

		Examples:
			|id|firstName|lastName|email|
			|1 |testGeorge|testBluth|testgeorge.bluth@reqres.in|

	@CheckDeleteUser
	Scenario: Verify deletion of an existing user
		When I delete the user with ID "1"
		Then A delete response should be displayed in the system


	@CheckListUser
	Scenario Outline: Check deleted user in full listing query
		When I delete the user with ID "<id>"
		And I consult the complete list of users
		Then The list of users should not contain a user with ID "<id>"

		Examples:
			|id|
			|8 |
  ```
   
  
---
## Instalación

Para instalar el proyecto deben seguir los siguientes pasos:
  
**1. Clonar el proyecto**

```
git clone https://gitlab.com/CristianJHdezZ/retosofka-autoapitesting-regres.git
```

**2. Importar el proyecto**
  
Para importar el proyecto debe hacerlo desde el IDE de su preferencia seleccionando la carpeta **"RetoSofka-AutoAPITesting-Regres-SerenityRest"** como un proyecto Gradle. 
  
  
**3. Ejecutar el proyecto** 

Para ejecutar el proyecto debe hacerlo a través de uno de los siguientes pasos: 
  
**3.1 A través de línea de comando**
  
Si se desea realizar a través de la línea de comando se debe ejecutar desde una consola de comandos (de su preferencia) con el comando: 

```
gradle clean test aggregate --info
```
  
**3.2 A través de IDE** 
  
Si se desea realizar la ejecución a través del IDE de desarrollo, se debe seleccionar la clase **"RegresInApiRunner.class"** y dar clic derecho, seleccionar la opción que permita correr la prueba. 
  

  
---
## ️Autores
* **Cristian Hernandez Z.** - [CristianHdezZ](https://github.com/CristianHdezZ/)
---

