package co.com.sofka.certification.api.stepsdefinitions;

import co.com.sofka.certification.api.model.Users;
import co.com.sofka.certification.api.questions.VerifyUserDetails;
import co.com.sofka.certification.api.tasks.ConsultUserByID;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;

import java.util.List;

import static co.com.sofka.certification.api.util.Constants.*;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class CheckUserIdSteps {
    @When("I consult the user by ID {string}")
    public void i_consult_the_user_by_id(String strId) {
        theActorInTheSpotlight().attemptsTo(ConsultUserByID.withTheFollowing(strId));
    }

    @Then("the user details should be returned")
    public void the_user_details_should_be_returned(List<Users> listUsers) {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(VerifyUserDetails.inTheAPIResultExpected(listUsers.get(FIRST_ITEM))));
    }

}
