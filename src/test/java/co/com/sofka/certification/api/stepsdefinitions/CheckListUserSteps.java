package co.com.sofka.certification.api.stepsdefinitions;

import co.com.sofka.certification.api.questions.VerifyUseNotExist;
import co.com.sofka.certification.api.questions.VerifyUserDetails;
import co.com.sofka.certification.api.tasks.GetListUsers;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;

import static co.com.sofka.certification.api.util.Constants.FIRST_ITEM;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class CheckListUserSteps {

    @When("I consult the complete list of users")
    public void i_consult_the_complete_list_of_users() {
        theActorInTheSpotlight().wasAbleTo(GetListUsers.inTheAPI());
    }
    @Then("The list of users should not contain a user with ID {string}")
    public void the_list_of_users_should_not_contain_a_user_with_id(String strId) {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(VerifyUseNotExist.inTheAPIResultExpected(strId)));
    }
}
