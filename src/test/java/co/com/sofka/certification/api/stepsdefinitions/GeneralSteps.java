package co.com.sofka.certification.api.stepsdefinitions;

import co.com.sofka.certification.api.model.Users;
import co.com.sofka.certification.api.tasks.GetListUsers;
import io.cucumber.java.Before;
import io.cucumber.java.DataTableType;
import io.cucumber.java.en.Given;
import io.restassured.RestAssured;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.thucydides.core.util.EnvironmentVariables;

import java.util.Map;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class GeneralSteps {
    EnvironmentVariables environmentVariables = null;

    @Before
    public void setUp() {
        OnStage.setTheStage(new OnlineCast());
        theActorCalled("UserTestAPI").whoCan(CallAnApi.at(EnvironmentSpecificConfiguration.from(environmentVariables).getProperty("my.webservice.endpoint")));

    }

    @DataTableType
    public Users userEntry(Map<String, String> entry){
        return new Users(entry.get("id"),entry.get("firstName"),entry.get("lastName"),entry.get("email"));
    }

    @Given("A list of users exists")
    public void a_list_of_users_exists() {
        theActorInTheSpotlight().wasAbleTo(GetListUsers.inTheAPI());

    }
}
