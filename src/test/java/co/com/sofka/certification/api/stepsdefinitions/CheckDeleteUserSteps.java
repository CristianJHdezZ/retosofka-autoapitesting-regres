package co.com.sofka.certification.api.stepsdefinitions;

import co.com.sofka.certification.api.questions.IsTheStatusCode;
import co.com.sofka.certification.api.tasks.DeleteUser;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static co.com.sofka.certification.api.util.Constants.STATUS_CODE_204;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.equalTo;

public class CheckDeleteUserSteps {

    @When("I delete the user with ID {string}")
    public void i_delete_the_user_with_id(String strId) {
        theActorInTheSpotlight().attemptsTo(DeleteUser.withTheFollowing(strId));
    }
    @Then("A delete response should be displayed in the system")
    public void a_delete_response_should_be_displayed_in_the_system() {
        theActorInTheSpotlight().should(seeThat("The status code of the response", IsTheStatusCode.correct(), equalTo(STATUS_CODE_204)));
    }
}
