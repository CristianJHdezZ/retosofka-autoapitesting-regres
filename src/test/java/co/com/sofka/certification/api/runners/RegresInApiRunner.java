package co.com.sofka.certification.api.runners;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty"},
        tags = "@AllTests",
        features = "src/test/resources/features/",
        glue = "co.com.sofka.certification.api.stepsdefinitions"
)
public class RegresInApiRunner {
}
