package co.com.sofka.certification.api.stepsdefinitions;

import co.com.sofka.certification.api.model.Users;
import co.com.sofka.certification.api.questions.VerifyUserDetails;
import co.com.sofka.certification.api.tasks.UpdateUser;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.GivenWhenThen;

import java.util.List;

import static co.com.sofka.certification.api.util.Constants.*;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class CheckUpdateUserSteps {

    @When("I update the user with ID {string} with the following details")
    public void i_update_the_user_with_id_with_the_following_details(String strId, List<Users> listUsers) {
        theActorInTheSpotlight().attemptsTo(UpdateUser.withTheFollowingData(strId, listUsers.get(FIRST_ITEM)));
    }

    @Then("the user details should be")
    public void the_user_details_should_be(List<Users> listUsers) {
        theActorInTheSpotlight().should(GivenWhenThen.seeThat(VerifyUserDetails.inTheAPIResultExpected(listUsers.get(FIRST_ITEM))));
    }
}
