@AllTests
Feature: Verify user API
  As a user
  I want to access reqres.in
  And manage the user API information

  Background: User exists
    Given A list of users exists

  @CheckUserId
  Scenario Outline: Check query of an existing user by ID
    When I consult the user by ID "<id>"
    Then the user details should be returned
      |id|firstName|lastName|email|
      |<id>|<firstName>|<lastName>|<email>|

  Examples:
  |id|firstName|lastName|email|
  |1 |George|Bluth|george.bluth@reqres.in|


  @CheckUpdateUser
  Scenario Outline: Check update the update of a user's data
    When I update the user with ID "<id>" with the following details
      |id| firstName| lastName| email |
      |<id>| <firstName> | <lastName>| <email> |
    Then the user details should be
      |id| firstName| lastName| email |
      |<id>| <firstName> | <lastName>| <email> |

    Examples:
      |id|firstName|lastName|email|
      |1 |testGeorge|testBluth|testgeorge.bluth@reqres.in|

  @CheckDeleteUser
  Scenario: Verify deletion of an existing user
    When I delete the user with ID "1"
    Then A delete response should be displayed in the system


  @CheckListUser
  Scenario Outline: Check deleted user in full listing query
    When I delete the user with ID "<id>"
    And I consult the complete list of users
    Then The list of users should not contain a user with ID "<id>"

    Examples:
      |id|
      |8 |