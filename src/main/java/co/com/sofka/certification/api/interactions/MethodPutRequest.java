package co.com.sofka.certification.api.interactions;

import co.com.sofka.certification.api.questions.IsTheStatusCode;
import io.restassured.http.ContentType;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Put;

import static co.com.sofka.certification.api.util.Constants.STATUS_CODE_200;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.equalTo;

public class MethodPutRequest implements Interaction {
    private final String strUrl;
    private final String strBody;

    public MethodPutRequest(String strUrl, String strBody) {
        this.strUrl = strUrl;
        this.strBody = strBody;
    }
    public static MethodPutRequest send(String strUrl, String strBody) {
        return Tasks.instrumented(MethodPutRequest.class,strUrl,strBody);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Put.to(strUrl).with(request -> request.contentType(ContentType.JSON)
                        .body(strBody).given().log().all())
        );
        theActorInTheSpotlight().should(seeThat("The status code of the response", IsTheStatusCode.correct(), equalTo(STATUS_CODE_200)));
    }
}