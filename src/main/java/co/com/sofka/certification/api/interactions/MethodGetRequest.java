package co.com.sofka.certification.api.interactions;

import co.com.sofka.certification.api.questions.IsTheStatusCode;
import io.restassured.http.ContentType;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Get;

import static co.com.sofka.certification.api.util.Constants.STATUS_CODE_200;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.Matchers.equalTo;

public class MethodGetRequest implements Interaction{
    private final String strUrl;

    public MethodGetRequest(String strUrl) {
        this.strUrl = strUrl;
    }

    public static MethodGetRequest send(String url) {
        return Tasks.instrumented(MethodGetRequest.class,url);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                Get.resource(strUrl).with(request -> request.contentType(ContentType.JSON)
                        .given().log().all())
        );
        theActorInTheSpotlight().should(seeThat("The status code of the response", IsTheStatusCode.correct(), equalTo(STATUS_CODE_200)));

    }
}
