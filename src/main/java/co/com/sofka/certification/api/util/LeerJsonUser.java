package co.com.sofka.certification.api.util;

import co.com.sofka.certification.api.model.Users;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import static co.com.sofka.certification.api.util.Constants.PATH_USER_BODY_JSON;

public class LeerJsonUser {
    private Users users;

    public LeerJsonUser(Users users) {
        this.users = users;
    }

    public static String BodyRequestUser(Users users){
        JSONObject jsUser = null;
        Object obUser;

        try {
            obUser = new JSONParser().parse(new FileReader(PATH_USER_BODY_JSON));
            jsUser = (JSONObject) obUser;
            jsUser.put("email",users.getEmail());
            jsUser.put("first_name",users.getFirstName());
            jsUser.put("last_name",users.getLastName());
        } catch (FileNotFoundException e) {
            System.err.println("El archivo no se encontró: " + e.getMessage());
        } catch (IOException e) {
            System.err.println("Hubo un error al leer el archivo: " + e.getMessage());
        } catch (ParseException e) {
            System.err.println("Hubo un error al parsear el archivo: " + e.getMessage());
        }

        return jsUser.toString();
    }

}
