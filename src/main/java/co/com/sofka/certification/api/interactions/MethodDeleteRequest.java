package co.com.sofka.certification.api.interactions;

import io.restassured.http.ContentType;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Delete;

public class MethodDeleteRequest implements Interaction {
    private final String strUrl;

    public MethodDeleteRequest(String strUrl) {
        this.strUrl = strUrl;
    }

    public static MethodDeleteRequest send(String url) {
        return Tasks.instrumented(MethodDeleteRequest.class,url);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Delete.from(strUrl).with(request -> request.contentType(ContentType.JSON)
                .given().log().all())
        );
    }
}
