package co.com.sofka.certification.api.util;

public class Constants {
    public static final String URL_All_USERS = "api/users?page=1";
    public static final String URL_USER = "api/users/%s";
    public static final String DATA_USER_ID = "USER";
    public static final String DATA_All_USER = "USER";
    public static final int STATUS_CODE_200 = 200;
    public static final int STATUS_CODE_204 = 204;
    public static final int FIRST_ITEM = 0;
    public static final String PATH_USER_BODY_JSON = "src/test/resources/DataJsonBodyRequest/user.json";

}
