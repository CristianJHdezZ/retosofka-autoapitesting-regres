package co.com.sofka.certification.api.questions;

import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;



public class VerifyUseNotExist implements Question<Boolean> {
    private String strId;

    public VerifyUseNotExist(String strId) {
        this.strId = strId;
    }

    public static VerifyUseNotExist inTheAPIResultExpected(String strId) {
        return new VerifyUseNotExist(strId);
    }

    @Override
    public Boolean answeredBy(Actor actor) {

        String bodyResult =  SerenityRest.lastResponse().jsonPath().getString("data.id");
        System.out.println("Lista: "+bodyResult);

        return  !bodyResult.contains(strId);


    }
}
