package co.com.sofka.certification.api.questions;

import co.com.sofka.certification.api.model.Users;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;

import static co.com.sofka.certification.api.util.Constants.DATA_USER_ID;

public class VerifyUserDetails implements Question<Boolean> {
    private Users listUsers;

    public VerifyUserDetails(Users listUsers) {
        this.listUsers = listUsers;
    }

    public static VerifyUserDetails inTheAPIResultExpected(Users listUsers) {
        return new VerifyUserDetails(listUsers);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        Users user = actor.recall(DATA_USER_ID);
        return user.getFirstName().equalsIgnoreCase(listUsers.getFirstName()) && user.getLastName().equalsIgnoreCase(listUsers.getLastName()) && user.getEmail().equalsIgnoreCase(listUsers.getEmail());
    }
}
