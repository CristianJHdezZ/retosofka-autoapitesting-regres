package co.com.sofka.certification.api.tasks;

import co.com.sofka.certification.api.interactions.MethodGetRequest;
import co.com.sofka.certification.api.model.Users;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;

import static co.com.sofka.certification.api.util.Constants.*;
import static co.com.sofka.certification.api.util.UserSerenityRest.UserResponse;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;

public class ConsultUserByID implements Task {
    private String strId;

    public ConsultUserByID(String strId) {
        this.strId = strId;
    }

    public static ConsultUserByID withTheFollowing(String strId) {
        return Tasks.instrumented(ConsultUserByID.class,strId);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                MethodGetRequest.send(String.format(URL_USER,strId))
        );
        actor.remember(DATA_USER_ID,UserResponse("GET"));
    }
}
