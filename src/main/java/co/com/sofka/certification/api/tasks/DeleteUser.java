package co.com.sofka.certification.api.tasks;

import co.com.sofka.certification.api.interactions.MethodDeleteRequest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;

import static co.com.sofka.certification.api.util.Constants.URL_USER;

public class DeleteUser implements Task {
    private String strId;

    public DeleteUser(String strId) {
        this.strId = strId;
    }

    public static DeleteUser withTheFollowing(String strId) {
        return Tasks.instrumented(DeleteUser.class,strId);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                MethodDeleteRequest.send(String.format(URL_USER,strId))
        );
    }
}
