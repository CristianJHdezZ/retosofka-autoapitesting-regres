package co.com.sofka.certification.api.util;

import co.com.sofka.certification.api.model.Users;
import net.serenitybdd.rest.SerenityRest;

public class UserSerenityRest {


    public static Users UserResponse(String kind){
        Users users = null;
        
        if (kind=="PUT"){
            users = new Users(
                    SerenityRest.lastResponse().jsonPath().getString("id"),
                    SerenityRest.lastResponse().jsonPath().getString("first_name"),
                    SerenityRest.lastResponse().jsonPath().getString("last_name"),
                    SerenityRest.lastResponse().jsonPath().getString("email")
            );         
        }
        else if (kind=="GET"){            
            users = new Users(
                    SerenityRest.lastResponse().jsonPath().getString("data.id"),
                    SerenityRest.lastResponse().jsonPath().getString("data.first_name"),
                    SerenityRest.lastResponse().jsonPath().getString("data.last_name"),
                    SerenityRest.lastResponse().jsonPath().getString("data.email")
            );
        }       
        

        return users;
    }


}
