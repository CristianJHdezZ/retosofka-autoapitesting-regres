package co.com.sofka.certification.api.tasks;

import co.com.sofka.certification.api.interactions.MethodGetRequest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;

import static co.com.sofka.certification.api.util.Constants.*;

public class GetListUsers implements Task {
    public static GetListUsers inTheAPI() {
        return Tasks.instrumented(GetListUsers.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                MethodGetRequest.send(URL_All_USERS)
        );
    }
}
