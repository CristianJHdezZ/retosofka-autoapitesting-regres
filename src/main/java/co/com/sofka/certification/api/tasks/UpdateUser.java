package co.com.sofka.certification.api.tasks;

import co.com.sofka.certification.api.interactions.MethodPutRequest;
import co.com.sofka.certification.api.model.Users;
import co.com.sofka.certification.api.util.LeerJsonUser;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;

import static co.com.sofka.certification.api.util.Constants.*;
import static co.com.sofka.certification.api.util.UserSerenityRest.UserResponse;


public class UpdateUser implements Task {
    private String strId;
    private Users users;

    public UpdateUser(String strId, Users users) {
        this.strId = strId;
        this.users = users;
    }

    public static UpdateUser withTheFollowingData(String strId, Users users) {
        return Tasks.instrumented(UpdateUser.class,strId,users);
    }


    @Override
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
                MethodPutRequest.send(String.format(URL_USER,strId), LeerJsonUser.BodyRequestUser(users))
        );

        actor.remember(DATA_USER_ID,UserResponse("PUT"));

    }
}
